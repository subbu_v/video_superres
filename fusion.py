import numpy as np
import tensorflow as tf
import cv2
import os
from input_data_reader import input_data_reader

class Model:
	''' model definition '''
	in_res = [1080-1,1920-1] ## after bicubic interpolation
	label_size = [1067,1907] ## high resolution 
	learning_rate= 1e-4
	MAX_ITER = 1000
	batch_size = 1
	checkpoint_dir = "checkpoints/"
	std_dev = 0.05

	def __init__(self):
		weight_parameters = []
		bias_parameters = []

		## high res color image at t+1 (this is the groundtruth)
		self.gt_place    = tf.placeholder(tf.float32, [None, self.label_size[0], self.label_size[1], 3], name='gt')

		### highres color image at t
		self.t_HR_input_place = tf.placeholder(tf.float32, [None, self.in_res[0], self.in_res[1], 3], name='t_HR_input_place')
		t_HR_conv1_weights = tf.Variable(tf.random_normal([9, 9, 3, 64], stddev=self.std_dev), name='t_HR_conv1_weights')
		t_HR_conv1_biases = tf.Variable(tf.zeros([64]), name='t_HR_conv1_biases')
		self.t_HR_conv1 = tf.nn.relu(tf.nn.conv2d(self.t_HR_input_place, t_HR_conv1_weights, strides=[1,1,1,1], padding='VALID') + t_HR_conv1_biases)
		weight_parameters += [t_HR_conv1_weights]
		bias_parameters += [t_HR_conv1_biases]

		### lowres grascale image at t
		self.t_LR_input_place = tf.placeholder(tf.float32, [None, self.in_res[0], self.in_res[1], 4], name='t_LR_input_place')
		t_LR_conv1_weights = tf.Variable(tf.random_normal([9, 9, 4, 64], stddev=self.std_dev), name='t_LR_conv1_weights')
		t_LR_conv1_biases = tf.Variable(tf.zeros([64]), name='t_LR_conv1_biases')
		self.t_LR_conv1 = tf.nn.relu(tf.nn.conv2d(self.t_LR_input_place, t_LR_conv1_weights, strides=[1,1,1,1], padding='VALID') + t_LR_conv1_biases)
		weight_parameters += [t_LR_conv1_weights]
		bias_parameters += [t_LR_conv1_biases]

		### lowres grascale image at t+1
		self.t_plus1_LR_input_place = tf.placeholder(tf.float32, [None, self.in_res[0], self.in_res[1], 4], name='t_plus1_LR_input_place')
		t_plus1_LR_conv1_weights = tf.Variable(tf.random_normal([9, 9, 4, 64], stddev=self.std_dev), name='t_plus1_LR_conv1_weights')
		t_plus1_LR_conv1_biases = tf.Variable(tf.zeros([64]), name='t_plus1_LR_conv1_biases')
		self.t_plus1_LR_conv1 = tf.nn.relu(tf.nn.conv2d(self.t_plus1_LR_input_place, t_plus1_LR_conv1_weights, strides=[1,1,1,1], padding='VALID') + t_plus1_LR_conv1_biases)
		weight_parameters += [t_plus1_LR_conv1_weights]
		bias_parameters += [t_plus1_LR_conv1_biases]
	
		## fuse them all, use concatenate; TODO: think about better way of fusion
		self.stacked_features = tf.concat(values=[self.t_HR_conv1, self.t_LR_conv1, self.t_plus1_LR_conv1], axis=3)


		## second half of SRCNN network
		self.weights = {
			#'w1': tf.Variable(tf.random_normal([9, 9, 1, 64], stddev=1e-3), name='w1'),
			'w2': tf.Variable(tf.random_normal([1, 1, 3*64, 32], stddev=self.std_dev), name='w2'),
			'w3': tf.Variable(tf.random_normal([5, 5, 32, 3],  stddev=self.std_dev), name='w3')
		}
		self.biases = {
			#'b1': tf.Variable(tf.zeros([64]), name='b1'),
			'b2': tf.Variable(tf.zeros([32]), name='b2'),
			'b3': tf.Variable(tf.zeros([3]), name='b3')
		}


		#conv1 = tf.nn.relu(tf.nn.conv2d(self.input_place, self.weights['w1'], strides=[1,1,1,1], padding='VALID') + self.biases['b1'])
		conv2 = tf.nn.relu(tf.nn.conv2d(self.stacked_features, self.weights['w2'], strides=[1,1,1,1], padding='VALID') + self.biases['b2'])
		weight_parameters += [self.weights['w2']]
		bias_parameters += [self.biases['b2']]

		self.pred = tf.nn.conv2d(conv2, self.weights['w3'], strides=[1,1,1,1], padding='VALID') + self.biases['b3']
		weight_parameters += [self.weights['w3']]
		bias_parameters += [self.biases['b3']]

		# Loss function (MSE)
		self.loss = tf.sqrt(tf.reduce_mean(tf.square(self.gt_place - self.pred)))

		# Stochastic gradient descent with the standard backpropagation
		#self.train_op = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(self.loss)
		self.global_step = tf.Variable(0)
		op1 = tf.train.GradientDescentOptimizer(0.0001)
		op2 = tf.train.GradientDescentOptimizer(0.0001*0.1)
		grads = tf.gradients(self.loss, weight_parameters + bias_parameters)
		grads1 = grads[:len(weight_parameters)]
		grads2 = grads[len(weight_parameters):]

		train_op1 = op1.apply_gradients(zip(grads1, weight_parameters), global_step = self.global_step)
		train_op2 = op2.apply_gradients(zip(grads2, bias_parameters), global_step = self.global_step)
		self.train_op = tf.group(train_op1, train_op2)


		self.saver = tf.train.Saver(weight_parameters + bias_parameters)

	def loadcheckpoints(self, sess):
		if os.path.exists(self.checkpoint_dir):
			print('[*] reading chcekpoints')
			ckpt = tf.train.get_checkpoint_state(self.checkpoint_dir)
			if ckpt:
				self.saver.restore(sess, ckpt.model_checkpoint_path)
				return True
			else:
				return False
		else:
			os.makedirs(self.checkpoint_dir)
			return False




	def infer(self, train_flag, train_iter=1000):
		self.MAX_ITER = train_iter
		sess = tf.Session()
		init = tf.global_variables_initializer()
		sess.run(init)
		
		if self.loadcheckpoints(sess):
			print("[*] load SUCCESS")
		else:
			print("[!] load FAILED")

		## if it is training phase, get minibatch of image and gt pairs
		if train_flag:
			print('training ------------------------------------------------------------->')
			data_reader = input_data_reader(train_flag=True)
			for i in range(self.MAX_ITER):
				batch_t_LR_images, batch_tplus1_LR_images, batch_t_HR_images, batch_GT = data_reader.next(self.batch_size)
				feed_dict = {
								self.t_HR_input_place:batch_t_HR_images,
								self.t_LR_input_place:batch_t_LR_images,
								self.t_plus1_LR_input_place:batch_tplus1_LR_images,
								self.gt_place:batch_GT
							}
				_, loss_value, step = sess.run([self.train_op, self.loss, self.global_step], feed_dict=feed_dict)
				batch_pred = sess.run(self.pred, feed_dict=feed_dict)
				img = cv2.cvtColor( np.uint8(batch_pred[0, :,:,:]*255), cv2.COLOR_YCrCb2BGR)
				#print(img.shape)
				cv2.imwrite('results/testing.png', img)

				if (i+1)%10==0:
					print("step:[%2d], loss:[%.8f]" \
						%( (i+1), loss_value ))
				if (i+1)%500 ==0:
					self.saver.save(sess, self.checkpoint_dir+"fusion_model", i+1)
			print('training is done')
		# # in testing phase, get the input images
		else:
			print('testing --------------------------------------------------->')
			test_data_reader = input_data_reader(train_flag=False)
			for i in range(4):
				batch_t_LR_images, batch_tplus1_LR_images, batch_t_HR_images, batch_GT = test_data_reader.next(1)
				feed_dict = {
								self.t_HR_input_place:batch_t_HR_images,
								self.t_LR_input_place:batch_t_LR_images,
								self.t_plus1_LR_input_place:batch_tplus1_LR_images,
								self.gt_place:batch_GT
							}
				batch_pred, loss_value = sess.run([self.pred, self.loss], feed_dict=feed_dict)
				# print(batch_pred[0])
				# img = test_data_reader.merge(batch_pred)
				print('error', loss_value)
				img = cv2.cvtColor( np.uint8(batch_pred[0, :,:,:]*255), cv2.COLOR_YCrCb2BGR)
				gt = cv2.cvtColor( np.uint8(batch_GT[0, :,:,:]*255), cv2.COLOR_YCrCb2BGR)
				cv2.imwrite('results/'+str(i)+'.png', np.concatenate([img, gt], axis=1))



net = Model()
<<<<<<< HEAD
# net.infer(train_flag=True, train_iter=100)
net.infer(train_flag=False)
=======
net.infer(train_flag=True, train_iter=15000)
net.infer(train_flag=False)
>>>>>>> 744c392638cc6c1399198d6691f65e7ff89d6475
