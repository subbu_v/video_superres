import cv2
import numpy as np
import os
import random

class input_data_reader:
	index=0
	def __init__(self, data_folder='data/samples/', train_flag=False, input_size=[1079,1919], gt_size=[1067,1907]):
		self.data_folder=data_folder
		self.train = train_flag
		self.input_size=input_size
		self.gt_size = gt_size
		# self.slide=slide
		if not os.path.exists(self.data_folder):
			print('data folder doesnot exists')
		else:
			files = os.listdir(self.data_folder)
			if len(files)==0:
				print('no files are found in the data folder. Copy the data first')
			else:
				if self.train:
					self.image_list = open('data/indoor_scenes.txt', 'r').readlines()
					random.shuffle(self.image_list)
				else:
					self.image_list = open('data/test.txt', 'r').readlines()
		
		#print(self.image_list)

	def next(self, batch_size=1):
		#print( len(self.train_list) )
		#print(self.index)
		if self.index + batch_size > len(self.image_list):
			print('one epoch is completed!')
			self.index = 0

		input_files = self.image_list[self.index:self.index+batch_size]		
		self.index += batch_size

		t_low_res_batch = []
		t_plus1_low_res_batch = []
		t_high_res_batch = []
		t_plus1_high_res_batch = []

		for name in input_files:
			frame_idx = 1
			if self.train:
				frame_idx = np.random.randint(1,179) ## assuming frame indexing starts from 1
			# print('frame index ',frame_idx) 
			name = self.data_folder+name.rstrip().split('/')[-1] 
			## high res low speed color image t
			total_file_name = name+'_HR_HS_cam_0001-0180.avi'
			print(total_file_name, frame_idx)
			video = cv2.VideoCapture(total_file_name)
			current_idx=0
			while True:
				current_idx+=1
				ret, frame = video.read()
				if current_idx == frame_idx:
					frame = frame[0:self.input_size[0], 0:self.input_size[1], :]
					frame = cv2.cvtColor(frame, cv2.COLOR_BGR2YCrCb)
					t_high_res_batch.append(frame)
					break


			t_low_res = []
			t_plus1_low_res = []
			## read low res video
			for LR_file_name in ['_LR_HS_cam_0_0001-0180.avi','_LR_HS_cam_1_0001-0180.avi', '_LR_HS_cam_2_0001-0180.avi', '_LR_HS_cam_3_0001-0180.avi']:
				total_file_name =name+LR_file_name
				# print(total_file_name)
				video = cv2.VideoCapture(total_file_name)
				f_num = 0
				while f_num<frame_idx+2:   #(frame_idx-1)*6+3:
					f_num+=1
					ret, frame = video.read()
					frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
					frame = cv2.resize(frame, dsize=(1920,1080), interpolation=cv2.INTER_CUBIC)
					frame = frame[0:self.input_size[0], 0:self.input_size[1]]
					# cv2.imshow('video', frame)
					# cv2.waitKey(10)
					if f_num==frame_idx: #     (frame_idx-1)*6+1:
						t_low_res.append(frame)
					if f_num==frame_idx+1: #     #(frame_idx-1)*6+2:
						t_plus1_low_res.append(frame)

			t_low_res_batch.append(t_low_res)
			t_plus1_low_res_batch.append(t_plus1_low_res)

			
			
			## high res high speed color (groudtruth)
			total_file_name = name+'_HR_HS_cam_0001-0180.avi'
			# print(total_file_name)
			video = cv2.VideoCapture(total_file_name)
			f_num = 0
			while f_num<frame_idx+2: #(frame_idx-1)*6+3:
				f_num+=1
				ret, frame = video.read()
				frame = frame[0+6:self.input_size[0]-6, 0+6:self.input_size[1]-6, :]
				# cv2.imshow('video', frame)
				# cv2.waitKey(10)
				if f_num==frame_idx+1: #:(frame_idx-1)*6+2:
					frame = cv2.cvtColor(frame, cv2.COLOR_BGR2YCrCb)
					t_plus1_high_res_batch.append(frame)
				
		if not self.train:
			## write the images for checking
			result_dir = 'results/'
			if not os.path.exists(result_dir):
				os.makedirs(result_dir)
			cv2.imwrite(result_dir+name.split('/')[-1]+'_HR_LS_t.png', np.uint8(t_high_res_batch[0]))
			cv2.imwrite(result_dir+name.split('/')[-1]+'_HR_HS_tplus1.png', np.uint8(t_plus1_high_res_batch[0]))
			for k in range(4):
				# print(np.array(t_plus1_low_res_batch).shape)
				cv2.imwrite(result_dir+name.split('/')[-1]+'_LR_HS_tplus1_'+ str(k) +'.png', np.uint8(t_plus1_low_res_batch[0][k]))
				cv2.imwrite(result_dir+name.split('/')[-1]+'_LR_HS_t_'+ str(k) +'.png', np.uint8(t_low_res_batch[0][k]))

		return self.tensorify(t_low_res_batch, t_plus1_low_res_batch, t_high_res_batch, t_plus1_high_res_batch )

	
	def tensorify(self, t_low_res_batch, t_plus1_low_res_batch, t_high_res_batch, t_plus1_high_res_batch):
		t_low_res_batch  = np.array(t_low_res_batch).transpose(0,2,3,1) /255.
		t_plus1_low_res_batch = np.array(t_plus1_low_res_batch).transpose(0,2,3,1) /255.
		t_high_res_batch   = np.array(t_high_res_batch) /255.
		t_plus1_high_res_batch = np.array(t_plus1_high_res_batch) /255.
		'''
		num_images = t_low_res_batch.shape[0]
		width = t_low_res_batch.shape[2]
		height = t_low_res_batch.shape[1]
		offset = int((self.input_size-1)/2)
		gt_offset = int((self.gt_size-1)/2)

		sub_t_low_res_batch       = []
		sub_t_plus1_low_res_batch = []
		sub_t_high_res_batch      = []
		sub_t_plus1_high_res_batch= []

		for i in range(num_images):
			for c in range(offset, width-offset, self.slide):
				for r in range(offset, height-offset, self.slide):
					img = t_low_res_batch[i,r-offset:r+offset+1,c-offset:c+offset+1,:]
					sub_t_low_res_batch.append(img)
					img = t_plus1_low_res_batch[i,r-offset:r+offset+1,c-offset:c+offset+1,:]
					sub_t_plus1_low_res_batch.append(img)
					img = t_high_res_batch[i,r-offset:r+offset+1,c-offset:c+offset+1,:]
					sub_t_high_res_batch.append(img)
					img = t_plus1_high_res_batch[i,r-gt_offset:r+gt_offset+1,c-gt_offset:c+gt_offset+1,:]
					sub_t_plus1_high_res_batch.append(img)
		
		return np.array(sub_t_low_res_batch), np.array(sub_t_plus1_low_res_batch), np.array(sub_t_high_res_batch), np.array(sub_t_plus1_high_res_batch)
		'''
		return t_low_res_batch, t_plus1_low_res_batch, t_high_res_batch, t_plus1_high_res_batch

	def merge(self, sub_images):
		width = 1920
		height = 1080
		img = np.zeros(shape=(height,width, 3), dtype=np.float)
		offset = int((self.input_size-1)/2)
		gt_offset = int((self.gt_size-1)/2)
		i=0
		for c in range(offset, width-offset, self.slide):
			for r in range(offset, height-offset, self.slide):
				#print(img[r-gt_offset:r+gt_offset+1,c-gt_offset:c+gt_offset+1,:].shape, sub_images[i,:,:,:].shape)
				#cv2.imshow('test', sub_images[i,:,:,:])
				#cv2.waitKey(10)
				img[r-gt_offset:r+gt_offset+1,c-gt_offset:c+gt_offset+1,:] = sub_images[i,:,:,:]
				i+=1
		return np.uint8(img*255)

	
	

### for checking the reader
# reader = input_data_reader(train_flag=True)
# for _ in range(100):
# 	t_LR, tplus1_LR, t_HR, tplus1_HR = reader.next(batch_size=1)
# 	print('size of t_LR tensor --> ', t_LR.shape)
# 	print('size of tplus1_LR tensor --> ', tplus1_LR.shape)
# 	print('size of t_HR tensor --> ', t_HR.shape)
# 	print('size of tplus_HR tensor --> ', tplus1_HR.shape)
# # 	for i in range(t_HR.shape[0]):
# 	img = tplus1_HR[0,:,:,:]
# 	cv2.imshow('img', img)

# 	if 0xFF & cv2.waitKey(100)==ord('q'):
# 		break