import os
import numpy as np
import sys

if not os.path.exists('blender/'):
  print("blender is not there,,, downloading it")
  blender_url = 'http://ftp.halifax.rwth-aachen.de/blender/release/Blender2.78/blender-2.78c-linux-glibc219-x86_64.tar.bz2'
  os.system('wget '+blender_url+' -O blender.tar.bz2')
  os.system('tar -xvf blender.tar.bz2')
  os.system('mv blender-2.78c-linux-glibc219-x86_64 blender')

if not os.path.exists('data/indoor_scene_layouts/'):
	print('downloading assets......')
	os.system('wget http://fias.uni-frankfurt.de/~subbu/indoor_scene_layouts.zip -P data/')
	os.system('unzip data/indoor_scene_layouts.zip -d data/')

assets_list = 'data/indoor_scenes.txt'
if not os.path.exists(assets_list):
	os.system('ls data/indoor_scene_layouts/*/*.obj > data/indoor_scenes.txt')

f = open(assets_list, 'r').readlines()

for line in f:
	obj_file = line.rstrip('\n')
	cmd = 'blender/blender -b -P script.py -- '+obj_file
	#print(cmd)
  #cmd = 'blender/blender -b -E CYCLES '+blend_file+' -P '+args.script
	os.system(cmd)
  
  
   
