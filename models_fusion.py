import numpy as np
import tensorflow as tf
import cv2
import os

from pre_train_batch_reader import pre_train_batch_reader

'''
the first model takes stacked frames t1,t2 from each cam
'''

class network:
	''' model definition '''
	# in_res = [1080-1,1920-1] ## after bicubic interpolation
	in_res = [720-1,1080-1]
	label_size = [in_res[0]-2*6,in_res[1]-2*6]  ## high resolution 
	learning_rate= 1e-4
	batch_size = 1
	checkpoint_dir = "checkpoints/"
	std_dev = 1e-2

	def __init__(self, encoder='stack'):

		weight_parameters = []
		bias_parameters = []

		## a place holder for groundtruth which is highspeed high res frame at t2
		self.gt_place = tf.placeholder(tf.float32, [None, self.label_size[0], self.label_size[1], 3], name='gt_place')

		## a place holder fot from low speed high res came at t1
		self.hr_ls_t1 = tf.placeholder(tf.float32, [None, self.in_res[0], self.in_res[1], 3], name='hr_ls_t1')
		hr_ls_t1_conv1_weights = tf.Variable(tf.random_normal([9, 9, 3, 64], stddev=self.std_dev), name='hr_ls_t1_conv1_weights')
		hr_ls_t1_conv1_biases = tf.Variable(tf.zeros([64]), name='hr_ls_t1_conv1_biases')
		self.hr_ls_t1_conv1 = tf.nn.relu(tf.nn.conv2d(self.hr_ls_t1, hr_ls_t1_conv1_weights, strides=[1,1,1,1], padding='VALID') + hr_ls_t1_conv1_biases)
		weight_parameters += [hr_ls_t1_conv1_weights]
		bias_parameters += [hr_ls_t1_conv1_biases]


		## if the encoding is just stacking frames t1 and t2
		if encoder == 'stack':
			self.checkpoint_dir = self.checkpoint_dir+'stack_encoding/'

			## placeholders for all four low res high speed cams
			## after stacking, they become 2 channel images
			self.lr_hs_cams = []
			self.lr_hs_conv1 = []
			for i in range(4):
				name = 'lr_hs_cam'+str(i+1)		
				placeholder = tf.placeholder(tf.float32, [None, self.in_res[0], self.in_res[1], 2], name=name)
				self.lr_hs_cams.append(placeholder)
				lr_hs_conv1_weights = tf.Variable(tf.random_normal([9, 9, 2, 64], stddev=self.std_dev), name=name+'_conv1_weights')
				lr_hs_conv1_biases = tf.Variable(tf.zeros([64]), name=name+'_conv1_biases')
				lr_hs_conv1_resp = tf.nn.relu(tf.nn.conv2d(placeholder, lr_hs_conv1_weights, strides=[1,1,1,1], padding='VALID') + lr_hs_conv1_biases)
				self.lr_hs_conv1.append(lr_hs_conv1_resp)
				weight_parameters += [lr_hs_conv1_weights]
				bias_parameters += [lr_hs_conv1_biases]

		## now stack all the conv features
		self.stacked_features = tf.concat(values=[self.hr_ls_t1_conv1, self.lr_hs_conv1[0], self.lr_hs_conv1[1], self.lr_hs_conv1[2], self.lr_hs_conv1[3] ], axis=3)

		## second half of SRCNN network
		self.weights = {
			#'w1': tf.Variable(tf.random_normal([9, 9, 1, 64], stddev=1e-3), name='w1'),
			'w2': tf.Variable(tf.random_normal([1, 1, 5*64, 32], stddev=self.std_dev), name='w2'),
			'w3': tf.Variable(tf.random_normal([5, 5, 32, 3],  stddev=self.std_dev), name='w3')
		}
		self.biases = {
			#'b1': tf.Variable(tf.zeros([64]), name='b1'),
			'b2': tf.Variable(tf.zeros([32]), name='b2'),
			'b3': tf.Variable(tf.zeros([3]), name='b3')
		}


		#conv1 = tf.nn.relu(tf.nn.conv2d(self.input_place, self.weights['w1'], strides=[1,1,1,1], padding='VALID') + self.biases['b1'])
		conv2 = tf.nn.relu(tf.nn.conv2d(self.stacked_features, self.weights['w2'], strides=[1,1,1,1], padding='VALID') + self.biases['b2'])
		weight_parameters += [self.weights['w2']]
		bias_parameters += [self.biases['b2']]

		self.pred = tf.nn.conv2d(conv2, self.weights['w3'], strides=[1,1,1,1], padding='VALID') + self.biases['b3']
		weight_parameters += [self.weights['w3']]
		bias_parameters += [self.biases['b3']]

		# Loss function (MSE)
		self.loss = tf.sqrt(tf.reduce_mean(tf.square(self.gt_place - self.pred)))

		# Stochastic gradient descent with the standard backpropagation
		#self.train_op = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(self.loss)
		self.global_step = tf.Variable(0)
		op1 = tf.train.GradientDescentOptimizer(0.0001)
		op2 = tf.train.GradientDescentOptimizer(0.0001*0.1)
		grads = tf.gradients(self.loss, weight_parameters + bias_parameters)
		grads1 = grads[:len(weight_parameters)]
		grads2 = grads[len(weight_parameters):]

		train_op1 = op1.apply_gradients(zip(grads1, weight_parameters), global_step = self.global_step)
		train_op2 = op2.apply_gradients(zip(grads2, bias_parameters), global_step = self.global_step)
		self.train_op = tf.group(train_op1, train_op2)


		self.saver = tf.train.Saver(weight_parameters + bias_parameters)

	def loadcheckpoints(self, sess):
		if os.path.exists(self.checkpoint_dir):
			print('[*] reading chcekpoints')
			ckpt = tf.train.get_checkpoint_state(self.checkpoint_dir)
			if ckpt:
				self.saver.restore(sess, ckpt.model_checkpoint_path)
				return True
			else:
				return False
		else:
			os.makedirs(self.checkpoint_dir)
			return False 

	def pretrain(self, iter=1000):
		sess = tf.Session()
		init = tf.global_variables_initializer()
		sess.run(init)
		if self.loadcheckpoints(sess):
			print('[*] load checkpoint SUCCESS')
		else:
			print('[!] load checkpoint FAILED')
		print('training--------------------->')
		pretrain_datareader = pre_train_batch_reader(train=True)
		loss_log = []
		for i in range(iter):
			hr_ls_t1_batch, ls_hs_cam1_batch, ls_hs_cam2_batch, ls_hs_cam3_batch, ls_hs_cam4_batch, hr_hs_t2_batch = pretrain_datareader.next_batch(encoder = 'stack')
			feed_dict = {
				self.hr_ls_t1:hr_ls_t1_batch,
				self.lr_hs_cams[0]:ls_hs_cam1_batch,
				self.lr_hs_cams[1]:ls_hs_cam2_batch,
				self.lr_hs_cams[2]:ls_hs_cam3_batch,
				self.lr_hs_cams[3]:ls_hs_cam4_batch,
				self.gt_place:hr_hs_t2_batch
			}

			_, loss_value, step = sess.run([self.train_op, self.loss, self.global_step], feed_dict=feed_dict)
			batch_pred = sess.run(self.pred, feed_dict=feed_dict)
			img = cv2.cvtColor( np.uint8(batch_pred[0, :,:,:]*255), cv2.COLOR_YCrCb2BGR)
			cv2.imwrite('results/testing.png', img)
			if (i+1)%1==0:
				print("step:[%2d], loss:[%.8f]" %( (i+1), loss_value ))
				loss_log.append((i, loss_value))

			if (i+1)%100 ==0:
				self.saver.save(sess, self.checkpoint_dir+"fusion_model", i+1)
		print('training is done')
		np.save('results/loss_log.npy', loss_log)
			


net= network(encoder='stack')
net.pretrain(iter=100)

