## image pretraining experiments
import cv2
import numpy as np


class pre_train_batch_reader:
	DATA_FOLDER = 'data/60fps_images_archieve/'
	def __init__(self, train=False):
		self.train = train
		if not self.train:
			self.index=0
	def next_batch(self, encoder='stack'):
		if self.train:
			scene_number = np.random.randint(0,5)
			frame_number = np.random.randint(0,299)


		if not self.train:
			scene_number = 5
			frame_number = self.index
			if not self.index < 300:
				print("frame index, "+str(self.index)+" exceeded max limit (299)")
				return
			self.index = self.index+1
		# print(scene_number, frame_number)
		filename_t1 = self.DATA_FOLDER+'scene_'+str(scene_number).zfill(2)+'_'+str(frame_number).zfill(4)+'.png'
		filename_t2 = self.DATA_FOLDER+'scene_'+str(scene_number).zfill(2)+'_'+str(frame_number+1).zfill(4)+'.png'
		in_res = (1080-1, 720-1) ## after bicubic interpolation
		img_t1 = cv2.imread(filename_t1, 1)
		img_t2 = cv2.imread(filename_t2, 1)
		hr_ls_t1 = cv2.cvtColor( cv2.resize(img_t1, dsize=in_res), cv2.COLOR_BGR2YCrCb)
		hr_hs_t2 = cv2.cvtColor( cv2.resize(img_t2, dsize=in_res), cv2.COLOR_BGR2YCrCb)
		hr_hs_t2 = hr_hs_t2[6:-6,6:-6]

		ls_hs_t1_cam1 = cv2.cvtColor( cv2.resize( img_t1[0:-6,3:-3], dsize=in_res), cv2.COLOR_BGR2GRAY)
		ls_hs_t1_cam2 = cv2.cvtColor( cv2.resize( img_t1[6:,3:-3], dsize=in_res), cv2.COLOR_BGR2GRAY)
		ls_hs_t1_cam3 = cv2.cvtColor( cv2.resize( img_t1[3:-3,0:-6], dsize=in_res), cv2.COLOR_BGR2GRAY)
		ls_hs_t1_cam4 = cv2.cvtColor( cv2.resize( img_t1[3:-3,6:], dsize=in_res), cv2.COLOR_BGR2GRAY)

		ls_hs_t2_cam1 = cv2.cvtColor( cv2.resize( img_t2[0:-6,3:-3], dsize=in_res), cv2.COLOR_BGR2GRAY)
		ls_hs_t2_cam2 = cv2.cvtColor( cv2.resize( img_t2[6:,3:-3], dsize=in_res), cv2.COLOR_BGR2GRAY)
		ls_hs_t2_cam3 = cv2.cvtColor( cv2.resize( img_t2[3:-3,0:-6], dsize=in_res), cv2.COLOR_BGR2GRAY)
		ls_hs_t2_cam4 = cv2.cvtColor( cv2.resize( img_t2[3:-3,6:], dsize=in_res), cv2.COLOR_BGR2GRAY)
		# print(hr_hs_t2.shape)
		# cv2.imshow('test', np.uint8(0.25*ls_hs_t1_cam1+0.25*ls_hs_t1_cam2+0.25*ls_hs_t1_cam3+0.25*ls_hs_t1_cam4))
		# cv2.waitKey(0)

			
		if encoder == 'stack':
			ls_hs_cam1_batch = np.dstack((ls_hs_t1_cam1, ls_hs_t2_cam1))
			ls_hs_cam2_batch = np.dstack((ls_hs_t1_cam2, ls_hs_t2_cam2))
			ls_hs_cam3_batch = np.dstack((ls_hs_t1_cam3, ls_hs_t2_cam3))
			ls_hs_cam4_batch = np.dstack((ls_hs_t1_cam4, ls_hs_t2_cam4))		

		hr_ls_t1_batch =  np.expand_dims(hr_ls_t1, axis=0) /255.
		hr_hs_t2_batch = np.expand_dims(hr_hs_t2, axis=0)  /225.

		ls_hs_cam1_batch =  np.expand_dims(ls_hs_cam1_batch, axis=0) /225.
		ls_hs_cam2_batch =  np.expand_dims(ls_hs_cam2_batch, axis=0) /225.
		ls_hs_cam3_batch =  np.expand_dims(ls_hs_cam3_batch, axis=0) /225.
		ls_hs_cam4_batch =  np.expand_dims(ls_hs_cam4_batch, axis=0) /225.

		return hr_ls_t1_batch, np.array(ls_hs_cam1_batch), np.array(ls_hs_cam2_batch), np.array(ls_hs_cam3_batch), np.array(ls_hs_cam4_batch), hr_hs_t2_batch


# reader = pre_train_batch_reader(train=True)
# for i in range(1000):
# 	reader.next_batch(batch_size=1)
