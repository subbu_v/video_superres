import bpy
import sys
import math
import random
import os
RAD = math.pi/180   # to convert degrees to radians

## use gpu for rendering
try:
	bpy.context.user_preferences.addons['cycles'].preferences.compute_device_type = 'CUDA'
	bpy.context.user_preferences.addons['cycles'].preferences.devices[0].use = True
	bpy.context.scene.cycles.device = 'GPU'
except:
	pass

### class for high res + 4 low-res cameras
class myCamCombo:
	def __init__(self, cam=None, offset=0.1):
		if cam is not None:            
			self.loc = cam.location[0:3]
			self.rot = cam.rotation_euler[0:3]
			self.scale = cam.scale

		## add our hybrid cam coordinate system
		bpy.ops.object.empty_add(type='PLAIN_AXES', location=(0,0,0), rotation=(0,0,0))
		bpy.context.object.name = 'CamCoord'
		self.cam_coord = bpy.data.objects['CamCoord']

		## add high res low speed camera
		bpy.ops.object.camera_add(location=(0,0,0), rotation=(0,0,0))
		bpy.context.object.name = 'HR_LS_cam'
		#bpy.context.camera.name = 'HR_LS_cam'
		bpy.context.object.parent = self.cam_coord
		self.highres_lowspeed_cam = bpy.data.objects['HR_LS_cam']

		## add other 4 cams
		self.lowres_highspeed_cams = []
		self.offsets = [(0,offset,0), (offset,0,0), (0,-offset,0), (-offset,0,0)]
		for i in range(4):
			#loc = (self.location[0]+self.offsets[i][0], self.location[1]+self.offsets[i][1], self.location[2]+self.offsets[i][2])
			bpy.ops.object.camera_add(location=self.offsets[i], rotation=(0,0,0))
			bpy.context.object.name = 'LR_HS_cam_'+str(i)
			bpy.context.object.scale = self.scale
			#bpy.context.camera.name = 'LR_HS_cam_'+str(i)
			bpy.context.object.parent = self.cam_coord
			self.lowres_highspeed_cams.append(bpy.context.object)

		## groundtruth camera : highres and highspeed cam
		bpy.ops.object.camera_add(location=(0,0,0), rotation=(0,0,0))
		bpy.context.object.name = 'HR_HS_cam'
		# bpy.context.camera.name = 'HR_HS_cam'
		bpy.context.object.parent = self.cam_coord
		self.highres_highspeed_cam = bpy.data.objects['HR_HS_cam']

		## now move total cam setup to orginal pos
		self.cam_coord.location = self.loc
		self.cam_coord.rotation_euler = self.rot
		self.highres_highspeed_cam.scale = self.scale
		self.highres_lowspeed_cam.scale = self.scale

	def keyframe_insert(self, frame_number, location, rotation=None, data_path='location', index=-1):
		bpy.context.scene.frame_set(frame_number)
		if location is not None:
			self.cam_coord.location = location
		if rotation is not None:
			self.cam_coord.rotation_eulers = rotation
		self.cam_coord.keyframe_insert(data_path=data_path, index=index)

	
	def anim_render(self, out_prefix, render=False):
		print('starting the render --------------------------------------------->')
		scene = bpy.context.scene

		## let us render with lowres high speed cams
		for cam in self.lowres_highspeed_cams:
			cam.data.sensor_width = 4.54
			cam.data.sensor_height = 3.42
			cam.data.lens = 3.85
			cam.data.sensor_fit = 'HORIZONTAL'
			
			scene.camera = cam
			# render settings
			engine = bpy.context.scene.render
			engine.filepath=out_prefix+cam.name+'_'
			engine.resolution_x = int(960/2)
			engine.resolution_y = int(540/2)
			engine.resolution_percentage=100
			engine.fps = 60
			scene.frame_end=180
			## motion blur
			engine.use_motion_blur = True
			engine.motion_blur_shutter = 0.5
			engine.motion_blur_samples = 1

			engine.image_settings.file_format = 'AVI_JPEG'
			if render:
				bpy.ops.render.render(animation=True)
		
		## now with highres high speed (ground truth)
		cam=self.highres_highspeed_cam
		cam.data.sensor_width = 11.1
		cam.data.sensor_height = 2.24
		crop_factor = 11.1/4.54
		cam.data.lens = 3.85*crop_factor
		cam.data.sensor_fit = 'HORIZONTAL'
		# scene = bpy.context.scene
		scene.camera = cam
		scene.frame_end=180
		# render settings
		engine = bpy.context.scene.render
		engine.filepath=out_prefix+cam.name+'_'
		engine.resolution_x = 960*2
		engine.resolution_y = 540*2
		engine.resolution_percentage=100
		engine.fps = 60

		engine.use_motion_blur = True
		engine.motion_blur_shutter = 0.5
		engine.motion_blur_samples = 1
		engine.image_settings.file_format = 'AVI_JPEG'
		if render:
			bpy.ops.render.render(animation=True)

		## now with highres low speed
		cam=self.highres_lowspeed_cam
		cam.data.sensor_width = 11.1
		cam.data.sensor_height = 2.24
		cam.data.lens = 3.85*crop_factor
		cam.data.sensor_fit = 'HORIZONTAL'
		# scene = bpy.context.scene
		scene.camera = cam

		## remap the animation
		'''
		scene.frame_set(60)
		end_loc = self.cam_coord.location
		scene.frame_set(1)
		start_loc = self.cam_coord.location
		self.cam_coord.animation_data_clear()
		self.keyframe_insert(frame_number=1, location=start_loc)
		self.keyframe_insert(frame_number=10, location=end_loc)
		'''
		scene.render.frame_map_old = 180
		scene.render.frame_map_new = 30
		scene.frame_end=30
		# render settings
		engine = bpy.context.scene.render
		engine.filepath=out_prefix+cam.name+'_'
		engine.resolution_x = 960*2
		engine.resolution_y = 540*2
		engine.resolution_percentage=100
		engine.fps = 10

		engine.use_motion_blur = True
		engine.motion_blur_shutter = 0.5*6
		engine.motion_blur_samples = 1
		engine.image_settings.file_format = 'AVI_JPEG'
		if render:
			bpy.ops.render.render(animation=True)


### remove all the objects in the scene
def clear_cache():
	# select default cube  and delete it 
	for o in bpy.data.objects:
		if o.type == 'MESH':
			o.select = True
		else:
			o.select = False
	# delete the selected object
	bpy.ops.object.delete()

clear_cache()

## get the model from args
argv = sys.argv
argv = argv[argv.index("--") + 1:] # get all args after "--"

scene_layout_name = argv[0]  # 3D cad file name 

## import obj file into blender
bpy.ops.import_scene.obj(filepath=scene_layout_name)
## use a exisiting camera and light
cam = bpy.data.objects['Camera']
light = bpy.data.objects['Lamp']

# adjust the camera and light
cam.scale = (0.3,0.3,0.3)
cam.location = (0,0,1.5)#(1.8, 0, 2)
cam.rotation_euler = (79.8*RAD, 0*RAD, 90*RAD)
bpy.data.cameras['Camera'].lens=10
light.location = (0,0,0.93)

# add more light to the scene
for i in range(-10,10,4):
	for j in range(-10, 10,4):
		bpy.ops.object.lamp_add(type='POINT', radius=1, location=(i,j,0.53))
for light in bpy.data.lamps:
	light.energy=0.3

# correct the textures of all material
for mat in bpy.data.materials:
	try:
		filepath = mat.active_texture.image.filepath
		texture_list = os.listdir(filepath)
		random_tex = random.choice(texture_list)
		#print(random_tex)
		bpy.ops.image.open(filepath=filepath+'/'+random_tex, directory=filepath)
		print(bpy.data.images[random_tex].name)
		mat.active_texture.image = bpy.data.images[random_tex]
	except:
		continue



scene = bpy.context.scene

## add 4 camera around active camera
active_camera = bpy.context.scene.camera

our_camera = myCamCombo(cam=active_camera)
bpy.context.scene.camera = our_camera.highres_lowspeed_cam

bpy.ops.object.select_all(action='DESELECT')
## make our cam rotate 360 degrees in 3 sec wrt z. 
camera_system= our_camera.cam_coord
camera_system.select = True
'''
camera_system.animation_data_create()
anim_data = camera_system.animation_data
anim_data.action = bpy.data.actions.new("cam_action")
'''

bpy.context.scene.frame_set(0)
rot = camera_system.rotation_euler
camera_system.keyframe_insert(data_path='rotation_euler', index=-1)
bpy.context.scene.frame_set(180)
rot[2] = rot[2] + 359*RAD
camera_system.rotation_euler = rot
camera_system.keyframe_insert(data_path='rotation_euler', index=-1)


prefix = scene_layout_name.split('/')[-1]
our_camera.anim_render(out_prefix='data/samples/'+prefix+'_', render=True)

